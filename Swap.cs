using System;
using System.Collections.Generic;
using System.Text;
 
using XRL.UI;
using XRL.Rules;
using XRL.Messages;
using XRL.World.Parts.Effects;
using ConsoleLib.Console;
 
namespace XRL.World.Parts.Mutation
{
    [Serializable]
    class Swap : BaseMutation
    {
        public Guid SwapActivatedAbilityID = Guid.Empty;

		public ActivatedAbilityEntry SwapActivatedAbility;


        public Swap()
        {
            DisplayName = "Swap";
            Type = "Mental";
        }
 
        public override void Register(GameObject Object)
        {
			// Object.RegisterPartEvent(this, "AIGetOffensiveMutationList");
			Object.RegisterPartEvent(this, "CommandSwap");
			base.Register(Object);  
        }
         
        public override string GetDescription()
        {
            return "Swap your location with that of your target.";
        }
 
        public override string GetLevelText(int Level)
        {
            return "You are able to swap your location with that of your target.";
        }

        public static bool Cast(Swap mutation = null, Event E = null, Cell TargetCell = null)
        {
            // test reality, idk this is in other spells
            if (!mutation.ParentObject.IsRealityDistortionUsable())
			{
				RealityStabilized.ShowGenericInterdictMessage(mutation.ParentObject);
				return true;
			}

            // test that we're not the player on the world map
            if (!mutation.ParentObject.IsPlayer() && mutation.ParentObject.pPhysics.CurrentCell.ParentZone.IsWorldMap())
            {
                Popup.Show("You may not teleport on the world map");
                return true;
            }

            TargetCell = mutation.PickDestinationCell(999, AllowVis.OnlyExplored, Locked: false);

            // test that the target is visible
            if (!TargetCell.IsVisible())
            {
                Popup.Show("You must have vision of your target");
                return true;
            }

            // find something in the cell to get
            GameObject targetObject = null;

            foreach (GameObject g in TargetCell.GetObjectsInCell())
            {
                if (g.HasPart("Combat"))
                    targetObject = g;
            }

            // for the mental mirror maybe just teleport the caster randomly a few squares?

            // test that the target isn't the caster
            if (targetObject == mutation.ParentObject)
            {
                Popup.Show("You can't swap with yourself!");
                return true;
            }

            // place the caster and the target in each other's locations
            targetObject.TeleportTo(mutation.ParentObject.CurrentCell);
            mutation.ParentObject.TeleportTo(TargetCell);
            
            return true;
        }
 
        public override bool FireEvent(Event E)
        {
            // TODO: put other stuff here for NPCs and whatever
            if (E.ID == "CommandSwap" && !Cast(this, E, E.GetParameter("Target") as Cell)) 
            {
                return false;
            }

            return true;
        }
 
        public override bool ChangeLevel(int NewLevel)
        {
            return true;
        }
 
        public override bool Mutate(GameObject GO, int Level)
        {
			Unmutate(GO);
			ActivatedAbilities part = GO.GetPart<ActivatedAbilities>();
			if (part != null)
			{
				SwapActivatedAbilityID = part.AddAbility("Swap", "CommandSwap", "Mental Mutation", string.Empty + '\u000f', IsRealityDistortionBased: true);
				SwapActivatedAbility = part.AbilityByGuid[SwapActivatedAbilityID];
			}
			return base.Mutate(GO, Level);

        }
 
        public override bool Unmutate(GameObject GO)
        {
			if (SwapActivatedAbilityID != Guid.Empty)
			{
				ActivatedAbilities part = GO.GetPart<ActivatedAbilities>();
				part.RemoveAbility(SwapActivatedAbilityID);
				SwapActivatedAbilityID = Guid.Empty;
			}
			return base.Unmutate(GO);
        }
    }
}